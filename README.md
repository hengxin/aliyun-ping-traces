# aliyun-ping-traces
Week-long ping logs across 9 Aliyun (阿里云) ECSes, spanning 3 datacenters in North China, South China, and East China.

It is about 4.4GB after uncompressed.

The 9 ECSes are:

- North China (nc, for short): nc1, nc2, and nc3
- South China (sc, for short): sc1, sc2, and sc3
- East China (ec, for short): ec1, ec2, and ec3

The ping log is of the form of: 
`[1467532783.251388] 64 bytes from nc1 (139.129.205.165): icmp_seq=1 ttl=56 time=17.4 ms`

For details, please refer to the [aliping project](https://github.com/hengxin/aliyun-projects/tree/master/aliyun-ecs-projects/aliyun-ecs-ping-project).

---

Later, I will analyze/visualize these raw data in various ways.

---

If you are interested in these data and/or have done something with them, please let me know (hengxin0912@gmail.com).
